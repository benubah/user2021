+++
title = "Sponsoring useR! 2021"
description = "Want to support the R Stats Language for Statistical Computing and its world class community? Learn how to help and benefit from the reach of the useR! conference."
keywords = ["conference","sponsor","support","open source"]
+++

# Learn How to Help and Benefit From the Reach of useR!

**The useR! conference** brings together users and developers of the R Language for Statistical Computing from around the world on a yearly basis. In 2021, the conference will be entirely virtual for the first time making the creation of a truly global, community-based, inclusive and innovative experience its number one priority. The organization committee has reimagined what can be, and is aiming to set a new bar for what should be. You as a sponsor, -- you should also be a part of this R-evolution.

**By sponsoring useR! 2021 you support the global R community and contribute to its ongoing development. It is an outstanding opportunity to reach a large group of highly talented data science professionals.**
 
# Your Spot at useR!
 
In various combinations and packages, we offer the following benefits to our sponsors:
 
- Free conference or tutorial attendees
 
- Logo on website, mentioning the sponsorship level
- Virtual Booth for interaction with participants
- Sponsor a Session or a Tutorial (Logo on slides between talks, mention at the beginning and end of Session/Tutorial)
- Day Sponsor: Banner/slide on screen during breaks, mentioned before keynotes
 
- Prime Time Sponsored Lightning Talk (<5min, just before the keynote)
- Sponsored Talk in Session (marked as sponsored)
- Recruitment or Company profile video (available in virtual booth and on the conference website)
 
- Social Media Posts (Twitter, LinkedIn)
 
 
# Why Sponsor a Virtual Event?
Fully virtual events are new to most of us. We leverage the virtual nature of the event to make it more accessible and more global. We want to make sure that you get the best out of your sponsoring engagement of this virtual event and are happy to discuss your suggestions. In every case, you have the opportunity to demonstrate commitment to R users around the world that have been ready to bring their knowhow forward to the global stage and have had limited opportunities to do so and to stand with a conference committed to ensuring accessibility in as many ways as possible.


Are you interested? - Please contact us via **useR2021-sponsoring [ at ] r-project.org**.

<br>
<br>
<br>
<br>