+++
title = "Global Organizing Committee"
description = "Information about the organisers"
keywords = ["about","organisers","staff","R consortium"]
+++

<div class="container" style="padding: 0">

  <div class="row pt-5">
    <div class="col-md-4 px-0 pr-4 pb-2 mb-5">
      <div class="mb-2">
        <h5 class="text-muted text-uppercase mb-4">Global Coordinators</h5>
        <p class="pb-2">Coordinate across teams, mentor different areas of the conference.</p>
      </div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong><a href="https://twitter.com/dickoah">AHMADOU DICKO</a></strong> technology, diversity</li>
<li class="py-4"><strong><a href="https://twitter.com/HugDorothea">DOROTHEA HUG PETER</a></strong> program, partners</li>
<li class="py-4"><strong><a href="https://twitter.com/whatsgoodio">MATT BANNERT</a></strong> comm., tech, finance</li>
<li class="py-4"><strong><a href="https://twitter.com/rocio_joo">ROCIO JOO</a></strong> program</li>
<li class="py-4"><strong><a href="https://twitter.com/heathrturnr">HEATHER TURNER</a></strong> R Foundation Link</li>
</ul>


</div>

<div class="col-md-4 px-0 pr-4 pb-2 mb-5">
<div class="">
<h5 class="text-muted text-uppercase mb-4">Program-n-Content</h5>
<p class="pb-2">Plans and implements the shape of the conference from tutorials to keynotes</p>
</div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong><a href="https://twitter.com/Fichulina">MARCELA ALFARO CÓRDOBA</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/yabellini">YANINA BELLINI SAIBENE</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/Nat_Mora_">NATALIA MORANDEIRA</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/rachelhey3">RACHEL HEYARD</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/_lacion_">LAURA ACIÓN</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/justfja">FOLAJIMI AROLOYE</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/FedeBioinfo">FEDERICO MARINI</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/josschavezf1">JOSELYN CHÁVEZ</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/lea_vanheyden">LEA SCHULZ-VANHEYDEN</a></strong></li>
</ul>


</div>

<div class="col-md-4 px-0 pr-4 pb-1 mb-5">
<div class="">
<h5 class="text-muted text-uppercase mb-4">Diversity</h5>
<p class="pb-2">Creates and ensures a welcoming, inclusive and diverse environment</p>
</div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong><a href="https://twitter.com/SanchezTapiaA">ANDREA SÁNCHEZ TAPIA</a></strong> team lead</li>
<li class="py-4"><strong><a href="https://twitter.com/_lacion_">LAURA ACIÓN</a></strong> scholarships</li>
<li class="py-4"><strong><a href="https://twitter.com/yabellini">YANINA BELLINI SAIBENE</a></strong> RLadies link</li>
<li class="py-4"><strong><a href="https://twitter.com/miljenka_vuko">MILJENKA VUKO</a></strong> Forwards link</li>
<li class="py-4"><strong><a href="https://www.linkedin.com/in/liz-hare-1a50925/">LIZ HARE</a></strong> Accessibility</li>
<li class="py-4"><strong><a href="https://twitter.com/josschavezf1">JOSELYN CHÁVEZ</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/Nat_Mora_">NATALIA MORANDEIRA</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/PaobCorrales">PAO CORRALES</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/Sciencificity">VEBASH NAIDOO</a></strong></li>



</ul>
</div>
</div>

<div class="row pt-5">
<div class="col-md-4 px-0 pr-4 pb-2 mb-5">
<div class="mb-2">
<h5 class="text-muted text-uppercase mb-4">Partners-n-Sponsors</h5>
<p class="pb-2">Finds and manages relationship to partners and backers of the conferences</p>
</div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong><a href="https://twitter.com/HugDorothea">DOROTHEA HUG PETER</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/murielburi">MURIEL BURI</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/yabellini">YANINA BELLINI SAIBENE</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/SanchezTapiaA">ANDREA SÁNCHEZ TAPIA</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/Shel_Kariuki">SHELMITH KARIUKI</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/spcanelon">SILVIA CANELÓN</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/AudrisCampbell">AUDRIS CAMPBELL</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/SindiaSherlyT">SHERLY TARAZONA</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/AdithiUpadhya">ADITHI R. UPADHYA</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/y__mattu">YUYA MATSUMURA</a></strong></li>
</ul>


</div>

<div class="col-md-4 px-0 pr-4 pb-2 mb-5">
<div class="">
<h5 class="text-muted text-uppercase mb-4">Technology</h5>
<p class="pb-2">Composes and manages the tech stack for a great virtual experience</p>
</div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong>FRANS VAN DUNNÉ</strong></li>
<li class="py-4"><strong><a href="https://twitter.com/rcentrrall">BEN UBAH</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/d_olivaw">ELIO CAMPITELLI </a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/justfja">FOLAJIMI AROLOYE</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/christoph_sax">CHRISTOPH SAX </a></strong> web</li>
<li class="py-4"><strong><a href="https://twitter.com/GordyPopovic">GORDANA POPOVIC </a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/pjs_228">PATRICK SCHRATZ </a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/lea_vanheyden">LEA SCHULZ-VANHEYDEN </a></strong></li>
</ul>



</div>

<div class="col-md-4 px-0 pr-4 pb-1 mb-5">
<div class="">
<h5 class="text-muted text-uppercase mb-4">Finance</h5>
<p class="pb-2">Budgets the event and coordinates accounting</p>
</div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong><a href="https://twitter.com/JakobDambon">JAKOB DAMBON</a></strong></li>
</ul>

</div>
</div>

<div class="row pt-5">
<div class="col-md-4 px-0 pr-4 pb-2 mb-5">
<div class="mb-2">
<h5 class="text-muted text-uppercase mb-4">Communication</h5>
<p class="pb-2">Keeps the R community up to date.</p>
</div>

<ul class="list-unstyled list-bordered text-xs-left my-4">
<li class="py-4"><strong>CORINNA GROBE</strong></li>
<li class="py-4"><strong>FRANCISCO ETCHART</strong>&nbsp;graphic design</li>
<li class="py-4"><strong><a href="https://twitter.com/rcentrrall">BEN UBAH</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/Shel_Kariuki">SHELMITH KARIUKI</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/BeaMilz">BEATRIZ MILZ</a></strong></li>
<li class="py-4"><strong><a href="https://twitter.com/miljenka_vuko">MILJENKA VUKO</a></strong></li>

</ul>


</div>
</div>
</div>

<br>
<br>
<br>
<br>